FROM node:15.8.0-alpine3.12

ENV NODE_ENV="production"

WORKDIR /service

COPY . .

RUN npm i --silent 

EXPOSE 80

CMD ["node", "build/index"]
