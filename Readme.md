# Auth Server

## Keys for JWT Tokens

If you dont't have any keys yet

```bash
ssh-keygen -t rsa -b 4096 -m PEM -f jwtRefreshToken.key
# Don't add passphrase
openssl rsa -in jwtRefreshToken.key -pubout -outform PEM -out jwtRefreshToken.key.pub
cat jwtRS256.key
cat jwtRS256.key.pub

```
