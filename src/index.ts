import dotenv from "dotenv";
import cors from "cors";
import fs from "fs";
import path from "path";
import { Request, Response } from "express";
import { MongoDBConnection } from "./config/connectToMongoDB";
import { login } from "./routes/auth/login";
import { logout } from "./routes/auth/logout";
import { refreshToken } from "./routes/auth/refreshToken";
import { verifyRole } from "./routes/auth/verifyRole";
import { ExpressServer } from "./server";
import { configureServerAndEnv } from "@joel-muehlena-website/jm-config-server-addon";
import { verifyJWTToken } from "./routes/auth/verifyJWTToken";
import cookieParser from "cookie-parser";

const configServer = `http://${
  process.env.NODE_ENV === "production"
    ? "config-server.default.svc.cluster.local"
    : "172.27.192.24"
}:8081/config?filename=auth:${
  process.env.NODE_ENV === "production" ? "prod" : "dev"
}`;

(async () => {
  try {
    await configureServerAndEnv(configServer, path.join(__dirname, "/.env"));
  } catch (err) {
    console.log("Error fetching config server");
  }
})();

let count = 0;
let i = setInterval(() => {
  if (count > 5000) {
    console.log("Config Timeout");
    process.exit(-1);
  }
  fs.access(path.join(__dirname, "/.env"), fs.constants.F_OK, (err) => {
    if (!err) {
      clearInterval(i);
      runApp();
    } else {
      count++;
    }
  });
}, 500);

const runApp = () => {
  dotenv.config({ path: path.join(__dirname, "/.env") });

  const PORT: number = parseInt(process.env.PORT as string) || 3001;

  let expressServer = new ExpressServer(PORT);

  /*const whitelist = [
    "localhost",
    "http://localhost:3000",
    "http://localhost:3002",
    "http://localhost:4200",
    "localhost:4200",
    "localhost:3002",
    "http://localhost:3003",
    "http://172.26.34.17:3000",
    "http://172.26.34.17:3002",
    "http://172.26.34.17:3002",
    "http://172.26.34.17:3003",
  ];*/

  const whitelistProd = [
    "https://joel.muehlena.de",
    "https://admin.joel.muehlena.de",
    "https://api.joel.muehlena.de",
    "https://assets.joel.muehlena.de",
    "https://blog.joel.muehlena.de",
    "https://login.joel.muehlena.de",
    "https://logging.api.joel.muehlena.de",
  ];

  const corsOptionsDelegate = function (req: any, callback: any) {
    let corsOptions;
    if (whitelistProd.indexOf(req.header("Origin")) !== -1) {
      corsOptions = { origin: true, credentials: true };
    } else {
      corsOptions = { origin: false, credentials: true };
    }
    callback(null, corsOptions);
  };

  expressServer.addMiddleware(cors, corsOptionsDelegate);
  expressServer.addMiddleware(cookieParser);

  //configure routes
  expressServer.get("/healthz", (_: Request, res: Response) => {
    res.sendStatus(200);
  });

  //Authentication
  expressServer.post("/auth/login", login);
  expressServer.get("/auth/token", refreshToken);
  expressServer.get("/auth/logout", logout);

  //Authorization and permission
  expressServer.post("/auth/verifyRole", verifyRole);
  expressServer.post("/auth/verifyToken", verifyJWTToken);

  //Run server
  expressServer.run();

  //Connect to Atlas cluster
  let dbConnection = new MongoDBConnection();
  dbConnection.connectToMongoDB();
};
