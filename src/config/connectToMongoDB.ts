import mongoose from "mongoose";

export class MongoDBConnection {
  /*private mongoUri = `mongodb+srv://joelMuehlena:${
    process.env.MONGODBPASSWORD as string
  }@joelmuehlenabackend.8fevt.mongodb.net/heroku_gxkt1fvl?retryWrites=true&w=majority&ssl=true`;*/
  private mongoUri = `mongodb://joelMuehlena:${
    process.env.MONGODBPASSWORD as string
  }@joelmuehlenabackend-shard-00-00.8fevt.mongodb.net:27017,joelmuehlenabackend-shard-00-01.8fevt.mongodb.net:27017,joelmuehlenabackend-shard-00-02.8fevt.mongodb.net:27017/heroku_gxkt1fvl?ssl=true&replicaSet=atlas-skyimi-shard-0&authSource=admin&retryWrites=true&w=majority`;

  private mongoOptions = {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  };

  connectToMongoDB = () => {
    mongoose
      .connect(this.mongoUri, this.mongoOptions)
      .then((_) => console.log("Connected to MongoDB Atlas Cluster"))
      .catch((err) => console.log(err));
  };
}
