import { User } from "./interfaces/User.interface";

import * as fs from "fs";

import * as jwt from "jsonwebtoken";

export const createJWTAccessToken = (user: User): string => {
  let payload = {
    type: "accessToken",
    id: user._id,
    roles: user.roles.join("%;;%"),
    username: user.username,
    firstName: user.firstName,
    lastName: user.lastName,
    avatar: user.avatar,
  };

  let pk = fs.readFileSync("keys/jwtAccessToken.key");

  return jwt.sign(payload, pk, { expiresIn: "15min", algorithm: "RS512" });
};

export const createJWTRefreshToken = (user: User): string => {
  let payload = {
    type: "refrshToken",
    userId: user._id,
    rev: user.sessionRev,
  };

  let pk = fs.readFileSync("keys/jwtRefreshToken.key");

  return jwt.sign(payload, pk, { expiresIn: "7 days", algorithm: "RS512" });
};
