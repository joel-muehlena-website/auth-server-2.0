import { Request, Response } from "express";
import { User } from "../../models/User";
import * as bcrypt from "bcrypt";
import { User as UserI } from "../../interfaces/User.interface";
import { Document } from "mongoose";
import {
  createJWTAccessToken,
  createJWTRefreshToken,
} from "../../createJWTToken";

interface loginError {
  username?: string;
  password?: string;
}

export const login = async (req: Request, res: Response) => {
  const { username, password } = req.body;

  let errors: loginError = {};

  if (!username) {
    errors.username = "Please provide a username";
  }

  if (!password) {
    errors.password = "Please provide a password";
  }

  if (Object.entries(errors).length > 0) {
    return res.status(401).json(errors);
  }

  let user: Document<UserI> | null = await User.findOne({ username });

  let isValid: boolean = false;

  if (!user || user === null) {
    errors.username = "User not found";
    return res.status(401).json(errors);
  } else {
    isValid = await bcrypt.compare(password, (user as any).password);
  }

  if (!isValid) {
    errors.password = "Wrong password. Please try again";
    return res.status(401).json(errors);
  }

  const accessToken = createJWTAccessToken(user as any);
  const refreshToken = createJWTRefreshToken(user as any);

  return res
    .cookie("refreshToken", `Bearer ${refreshToken}`, {
      httpOnly: true,
      secure: true,
      sameSite: "strict",
      domain: "muehlena.de",
      expires: new Date(Date.now() + 7 * (24 * 3600000)),
    })
    .json({ status: 200, token: `Bearer ${accessToken}` });
};
