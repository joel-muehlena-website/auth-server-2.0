import { Request, Response } from "express";

export const logout = (_: Request, res: Response) => {
  res
    .clearCookie("refreshToken", {
      httpOnly: true,
      secure: true,
      sameSite: "strict",
      domain: "muehlena.de",
    })
    .json({ code: 200, msg: "Logout successful" });
};
