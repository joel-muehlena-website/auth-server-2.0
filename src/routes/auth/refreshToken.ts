import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import * as fs from "fs";
import { createJWTAccessToken } from "../../createJWTToken";
import { User } from "../../models/User";
import { DecodedJWTRefreshToken } from "src/interfaces/Token.interface";

export const refreshToken = async (req: Request, res: Response) => {
  let { refreshToken } = req.cookies;
  const isSilentQuery = req.query.silent;
  const refreshTokenBody = req.body.refreshToken;

  const isSilent = isSilentQuery === "true" ? true : false;

  let statusCode = 400;

  if (!refreshToken && !refreshTokenBody) {
    statusCode = isSilent ? 200 : 401;

    return res.status(statusCode).json({
      error: "Please provide a refresh token. You have to login to get one",
    });
  }

  if (refreshTokenBody) {
    refreshToken = refreshTokenBody;
  }

  let decoded: DecodedJWTRefreshToken;
  const publicKey = fs.readFileSync("keys/jwtRefreshToken.key.pub");

  try {
    decoded = jwt.verify(refreshToken.replace("Bearer", "").trim(), publicKey, {
      algorithms: ["RS512"],
    }) as DecodedJWTRefreshToken;
  } catch (error) {
    statusCode = isSilent ? 200 : 401;
    return res.status(statusCode).clearCookie("refreshToken").json({
      error:
        "Please provide a valid refresh token. You have to login to get one",
    });
  }

  const user = await User.findById(decoded.userId);
  const sessionRev = (user as any).sessionRev;

  if (sessionRev !== decoded.rev) {
    statusCode = isSilent ? 200 : 401;
    return res.status(statusCode).clearCookie("refreshToken").json({
      error: "This token was revoked! Please login again.",
    });
  }

  const accessToken = createJWTAccessToken(user as any);
  return res.json({ status: 200, token: `Bearer ${accessToken}` });
};
