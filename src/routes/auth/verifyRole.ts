import { Request, Response } from "express";
import { IRole } from "../../interfaces/Role.interface";
import { Role } from "../../models/Roles";

interface RequestBody {
  roleName: string;
  permission: string;
}

export const verifyRole = async (
  req: Request<any, any, RequestBody>,
  res: Response
) => {
  let { permission, roleName } = req.query;
  const roleNameBody = req.body.roleName;
  const permissionBody = req.body.permission;

  if (!permission && permissionBody) {
    permission = permissionBody;
  }

  if (!roleName && roleNameBody) {
    roleName = roleNameBody;
  }

  if (!permission || !roleName) {
    return res.status(401).json({
      code: 401,
      msg: "Please proide a requested permission and a given role array",
    });
  }

  const userRolesFull: Array<string> = (roleName as string).split("%;;%");

  if (userRolesFull.length === 0) {
    return res.status(401).json({
      code: 401,
      msg: "Please proide a requested permission and a given role array",
    });
  }

  const userRoles: Array<string> = userRolesFull.map((el) => {
    const splitted = el.split(":");
    if (splitted.length === 1) {
      return splitted[0];
    } else {
      return splitted[1];
    }
  });

  const roleQuery = await Role.find({
    name: { $in: userRoles },
  });

  if (!roleQuery) {
    return res.status(401).json({
      code: 401,
      msg: "Please proide a valid and existing role, you may contact an admin",
    });
  }

  const userScopes: Array<string> = userRolesFull.map((el) => {
    const splitted = el.split(":");
    if (splitted.length === 2) {
      return splitted[0];
    } else {
      return "";
    }
  });

  permission = (permission as string).trim();
  const requestedPermission = (permission as string).split(":");

  const requestedPermissionScope: string =
    requestedPermission.length === 3 ? requestedPermission[0] : "";

  if (requestedPermission.length > 3 || requestedPermission.length <= 0) {
    return res.status(401).json({
      code: 401,
      msg: "Please proide a valid requested permission",
    });
  }

  console.log(userRolesFull);
  console.log(permission);

  for (let i = 0; i < roleQuery.length; ++i) {
    const queriedRole: IRole = roleQuery[i];

    let hasScope: boolean = false;
    for (let j = 0; j < userRolesFull.length; ++j) {
      if (
        queriedRole.scope === userScopes[j] &&
        queriedRole.scope === requestedPermissionScope
      ) {
        hasScope = true;
        break;
      }
    }

    if (!hasScope) continue;

    if (queriedRole.name === "admin") {
      return res.status(200).json({ code: 200, msg: "Access granted" });
    }

    let permissionName: string = "";
    let permissionValue: string = "";

    if (requestedPermission.length === 3) {
      permissionName = requestedPermission[1];
      permissionValue = requestedPermission[2];
    } else if (requestedPermission.length === 2) {
      permissionName = requestedPermission[0];
      permissionValue = requestedPermission[1];
    }

    if (queriedRole.permissions.has(permissionName)) {
      if (
        queriedRole.permissions.get(permissionName)!.includes(permissionValue)
      ) {
        return res.status(200).json({ code: 200, msg: "Access granted" });
      }
    }
  }

  return res.status(401).json({
    code: 401,
    msg: "Error not enough rights to access this ressource. Please contact your admin",
  });
};
