import { Request, Response } from "express";
import fs from "fs";

import jwt from "jsonwebtoken";

export const verifyJWTToken = async (req: Request, res: Response) => {
  const cert = fs.readFileSync("keys/jwtAccessToken.key.pub");

  const token = req.body.token;

  jwt.verify(token, cert, (err: any) => {
    if (err) {
      res.status(401).json({ code: 401, msg: "Token is invalid" });
    } else {
      res.setHeader("x-isAuthTokenValid", "true");
      res.status(200).json({ code: 200, msg: "Token is valid" });
    }
  });
};
