import * as mongoose from "mongoose";

const RoleScheme = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  scope: {
    type: String,
    default: "",
  },
  permissions: {
    type: Map,
    of: [String],
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    defualt: Date.now,
  },
});

export const Role = mongoose.model("Role", RoleScheme, "rolesV3");
