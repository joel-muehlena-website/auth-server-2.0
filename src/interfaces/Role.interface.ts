export interface IRole {
  name: string;
  scope: string;
  permissions: Map<string, Array<string>>;
}
