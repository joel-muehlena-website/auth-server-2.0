export interface DecodedJWTRefreshToken {
  userId: string;
  rev: number;
}
