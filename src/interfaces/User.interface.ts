export interface User {
  _id: string;
  firstName: String;
  lastName: String;
  email: String;
  username: String;
  avatar: String;
  sessionRev: Number;
  roles: Array<string>;
  password: String;
  createdAt: Date;
  updatedAt: Date;
}
